export default function getEnv(name:string) {
    const windowAny = window as any
    return windowAny?.configs?.[name] || process.env[name]
}