<?php

namespace App\Services;

use Exception;

class WorkerStatusService extends AbstractStatusService
{
    public function name(): string
    {
        return 'worker';
    }

    public function currentDefaultDriver(): string
    {
        return 'fake-worker';
    }

    protected function accessTry(): string|Exception|null
    {
        if (exec('ping -c 1 fake-worker; echo $?') == 0) {
            return null;
        }

        return new Exception('Worker is not running');
    }
}
