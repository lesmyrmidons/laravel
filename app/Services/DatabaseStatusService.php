<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\DB;

class DatabaseStatusService extends AbstractStatusService
{
    public function name(): string
    {
        return 'database';
    }

    public function currentDefaultDriver(): string
    {
        return DB::getDriverName();
    }

    protected function accessTry(): string|Exception
    {
        try {
            return DB::select(DB::raw('SELECT version() as version;'))[0]->version;
        } catch (Exception $e) {
            return $e;
        }
    }
}
